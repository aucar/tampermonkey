// ==UserScript==
// @name         AHM Internations
// @namespace    https://bitbucket.org/aucar/tampermonkey/raw/master/internations.tamper.js
// @version      0.1
// @description  Lists in an additional column the friend counts of users
// @author       You
// @match        http*://www.internations.org/members/*
// @match        http*://www.internations.org/members/mymatches/*
// @grant        none
// @require http://code.jquery.com/jquery-latest.js
// ==/UserScript==

$('#member-hitlist div.table-container  table  tbody  tr.grid-row-titles').append('<th>AHM Information</th>');


var profiles = new Array();
var elements = $('#member-hitlist > div.table-container > table > tbody > tr.grid-row-cells');
elements.each(function() { 

    var thisrow = $(this);
    
    var profurl = $(thisrow).find('li.user_info1 > a').get(0).href; 
    $(thisrow).append('<td><div class="friend-dummy" style="display:none;"></div><div class="friend-info"></div></td>');   
    
           
    $.get(profurl, function(data) {

  		var fricount = $(data).find('#profile-sidebar > div.in-box.in-box-simple.box-my-network > p.direct-contacts > strong').text();
        $(thisrow).find('.friend-info').html(fricount);
        
        if (fricount > 80) {
   			 $(thisrow).find('.friend-info').css('font-weight','bold').css('color','red');
		}

        //Make onlines green        
        var onlinestatus = $(data).find('#profile-header > div.column2 > ul > li.username > span.online-status').text();
        $(thisrow).find('.friend-info').append('<br />' + onlinestatus)        

        if (onlinestatus == 'online') {
   			 $(thisrow).find('.friend-info').css('font-weight','bold').css('color','green');
		}

	});

});


